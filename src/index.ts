// module de Node.js qui permet de lire les entrées utilisateur depuis la ligne de commande
import * as readlineSync from "readline-sync";

//test
console.log("coucou");

// fonction simple addition de 2 nombres
function add(a: number, b: number): number {
  let result: number = a + b;
  return result;
}
console.log(add(5, 6));

//taux de conversion
let euroCad: number = 1.5;
let euroJpy: number = 130;
let cadJpy: number = 87;

// conversion
function convert(deviseEntree: string, deviseSortie: string, montant: number) {
  if (deviseEntree === "CAD" && deviseSortie === "EUR") {
    return montant * (1 / euroCad) + "EUR";
  } else if (deviseEntree === "EUR" && deviseSortie === "CAD") {
    return montant * euroCad + "CAD";
  } else if (deviseEntree === "EUR" && deviseSortie === "JPY") {
    return montant * euroJpy + "JPY";
  } else if (deviseEntree === "JPY" && deviseSortie === "EUR") {
    return montant * (1 / euroJpy) + "EUR";
  } else if (deviseEntree === "JPY" && deviseSortie === "CAD") {
    return montant * (1 / cadJpy) + "CAD";
  } else if (deviseEntree === "CAD" && deviseSortie === "JPY") {
    return montant * cadJpy + "JPY";
  } else {
    return "Conversion de devises non prise en charge";
  }
}
console.log(convert("EUR", "CAD", 100)); // 150 EUR
console.log(convert("EUR", "JPY", 50)); //6500 JPY

// frais de livraison V1
/*function calculator(poids: number, destination: string) {
  if (poids <= 1 && destination === "France") {
    return "le prix de la livraison est 10 EUR";
  } else if (poids <= 1 && destination === "Canada") {
    return "le prix de la livraison est 15 CAD";
  } else if (poids <= 1 && destination === "Japon") {
    return "le prix de la livraison est 1000 JPY";
  } else if (poids > 1 && poids <= 3 && destination === "France") {
    return "le prix de la livraison est 20 EUR";
  } else if (poids > 1 && poids <= 3 && destination === "Canada") {
    return "le prix de la livraison est 30 EUR";
  } else if (poids > 1 && poids <= 3 && destination === "Japon") {
    return "le prix de la livraison est 2000 JPY";
  } else if (poids > 3 && destination === "France") {
    return "le prix de la livraison est 30 EUR";
  } else if (poids > 3 && destination === "Canada") {
    return "le prix de la livraison est 45 CAD";
  } else if (poids > 3 && destination === "Japon") {
    return "le prix de la livraison est 3000 JPY";
  } else {
    console.error("Frais de port non définis");
    return undefined;
  }
}

console.log(calculator(1, "France")); //10 euros
console.log(calculator(5, "Japon")); // 3000 JPY
*/

// frais de livraison V2
function calculatorTwo(poids: number, destination: string) {
  if (destination === "France") {
    if (poids <= 1) {
      return "le prix de la livraison est 10 EUR";
    } else if (poids > 1 && poids <= 3) {
      return "le prix de la livraison est 20 EUR";
    } else if (poids > 3) {
      return "le prix de la livraison est 30 EUR";
    }
  } else if (destination === "Canada") {
    if (poids <= 1) {
      return "le prix de la livraison est 15 CAD";
    } else if (poids > 1 && poids <= 3) {
      return "le prix de la livraison est 30 CAD";
    } else if (poids > 3) {
      return "le prix de la livraison est 45 CAD";
    }
  } else if (destination === "Japon") {
    if (poids <= 1) {
      return "le prix de la livraison est 1000 JPY";
    } else if (poids > 1 && poids <= 3) {
      return "le prix de la livraison est 2000 JPY";
    } else if (poids > 3) {
      return "le prix de la livraison est 3000 JPY";
    }
  } else {
    return "Frais de port non définis";
  }
}

console.log(calculatorTwo(1, "France")); //10 euros
console.log(calculatorTwo(5, "Japon")); // 3000 JPY

//todo Ajoute un supplément de 5 EUR, 7.5 CAD, 500 JPY pour les colis dont la somme des dimensions dépasse 150 cm.

// frais de douanes
function douaneTaxes(objectValue: number, country: string) {
  if (country === "france") {
    return "pas de frais de douane en France";
  } else if (objectValue >= 15 && country === "Canada") {
    let taxeFees = objectValue * 0.15;
    return `les frais de douanes sont de ${taxeFees} CAD supplémentaires.`;
  } else if (objectValue >= 5000 && country === "Japon") {
    let taxeFees = objectValue * 0.1;
    return `les frais de douanes sont de ${taxeFees} JPY supplémentaires.`;
  } else {
    return "Pas de frais de douane.";
  }
}

console.log(douaneTaxes(22, "france"));
console.log(douaneTaxes(5600, "Japon"));

// gestion des entrées utilisateur se fera via la ligne de commande

function main() {
  // Demander à l'utilisateur de choisir l'opération
  const readlineSync = require("readline-sync");
  const operation = readlineSync.question(
    "Choisissez une opération (1 pour convertir devises, 2 pour calculer frais de livraison, 3 pour calculer frais de douane): "
  );
  if (operation === "1") {
    // Opération de conversion de devises
    const montant: number = parseFloat(
      readlineSync.question("Entrez le montant à convertir: ")
    );
    const deviseEntree: string = readlineSync.question(
      "Entrez la devise de départ (EUR, CAD, JPY): "
    );
    const deviseSortie = readlineSync.question(
      "Entrez la devise de destination (EUR, CAD, JPY): "
    );
    console.log(deviseEntree, deviseSortie, montant)
    const montantConverti = convert(deviseEntree, deviseSortie, montant);
    console.log(
      `${montantConverti} ${deviseSortie}.`
    );
  } else if (operation === "2") {
    const poids = parseFloat(
      readlineSync.question("Entrez le poids du colis en kg: ")
    );
    const destination = readlineSync.question(
      "Entrez le pays de destination (France, Canada, Japon): "
    );
    const fraisLivraison = calculatorTwo(poids, destination);
    console.log(
      `${fraisLivraison}`
    );
  } else if (operation === "3") {
    const objectValue = parseFloat(
      readlineSync.question("Entrez la valeur déclarée du colis: ")
    );
    const country = readlineSync.question(
      "Entrez le pays de destination (France, Canada, Japon): "
    );

    const fraisDouane = douaneTaxes(objectValue, country);
    console.log(`${fraisDouane} ${country}.`);
  } else {
    console.log("Opération non reconnue.");
  }
}
main();
