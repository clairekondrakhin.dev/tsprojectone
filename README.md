 Application de Gestion de Logistique Internationale

Ce projet est une application en ligne de commande développée en TypeScript. Elle permet de gérer les conversions de devises, de calculer les frais de livraison, et d'estimer les frais de douane pour les colis dans le contexte de la logistique internationale.

## Installation

1. Assurez-vous d'avoir Node.js installé sur votre machine. Si ce n'est pas le cas, vous pouvez le télécharger sur https://nodejs.org/en/download/current.

2. Clonez ce dépôt Git sur votre machine :
https://gitlab.com/clairekondrakhin.dev/tsprojectone

Accédez au répertoire du projet :
cd nomduRepertoire

Installez les dépendances du projet en exécutant la commande :
npm ci

## Utilisation de l'application

L'application est composée de trois fonctionnalités principales : la conversion de devises et le calcul des frais de livraison et des frais de douanes.

Lorsque vous lancez l'application avec la commande "node dist" dans le terminal la question suivante va s'afficher:
"Choisissez une opération (1 pour convertir devises, 2 pour calculer frais de livraison, 3 pour calculer frais de douane): "

1. Conversion 
Lorsque vous tapez 1 les questions suivantes vont s'afficher :
"Entrez le montant à convertir: "
"Entrez la devise de départ (EUR, CAD, JPY): "
 "Entrez la devise de destination (EUR, CAD, JPY): "


2. frais de livraison
Lorsque vous tapez 2 les questions suivantes vont s'afficher :
"Entrez le poids du colis en kg: "
"Entrez le pays de destination (France, Canada, Japon): "

3. Frais de douane
Lorsque vous tapez 2 les questions suivantes vont s'afficher :
"Entrez la valeur déclarée du colis: "
"Entrez le pays de destination (France, Canada, Japon): "