"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
//test
console.log("coucou");
// fonction simple addition de 2 nombres
function add(a, b) {
    var result = a + b;
    return result;
}
console.log(add(5, 6));
//taux de conversion
var euroCad = 1.5;
var euroJpy = 130;
var cadJpy = 87;
// conversion
function convert(deviseEntree, deviseSortie, montant) {
    if (deviseEntree === "CAD" && deviseSortie === "EUR") {
        return montant * (1 / euroCad) + "EUR";
    }
    else if (deviseEntree === "EUR" && deviseSortie === "CAD") {
        return montant * euroCad + "CAD";
    }
    else if (deviseEntree === "EUR" && deviseSortie === "JPY") {
        return montant * euroJpy + "JPY";
    }
    else if (deviseEntree === "JPY" && deviseSortie === "EUR") {
        return montant * (1 / euroJpy) + "EUR";
    }
    else if (deviseEntree === "JPY" && deviseSortie === "CAD") {
        return montant * (1 / cadJpy) + "CAD";
    }
    else if (deviseEntree === "CAD" && deviseSortie === "JPY") {
        return montant * cadJpy + "JPY";
    }
    else {
        return "Conversion de devises non prise en charge";
    }
}
console.log(convert("EUR", "CAD", 100)); // 150 EUR
console.log(convert("EUR", "JPY", 50)); //6500 JPY
// frais de livraison V1
/*function calculator(poids: number, destination: string) {
  if (poids <= 1 && destination === "France") {
    return "le prix de la livraison est 10 EUR";
  } else if (poids <= 1 && destination === "Canada") {
    return "le prix de la livraison est 15 CAD";
  } else if (poids <= 1 && destination === "Japon") {
    return "le prix de la livraison est 1000 JPY";
  } else if (poids > 1 && poids <= 3 && destination === "France") {
    return "le prix de la livraison est 20 EUR";
  } else if (poids > 1 && poids <= 3 && destination === "Canada") {
    return "le prix de la livraison est 30 EUR";
  } else if (poids > 1 && poids <= 3 && destination === "Japon") {
    return "le prix de la livraison est 2000 JPY";
  } else if (poids > 3 && destination === "France") {
    return "le prix de la livraison est 30 EUR";
  } else if (poids > 3 && destination === "Canada") {
    return "le prix de la livraison est 45 CAD";
  } else if (poids > 3 && destination === "Japon") {
    return "le prix de la livraison est 3000 JPY";
  } else {
    console.error("Frais de port non définis");
    return undefined;
  }
}

console.log(calculator(1, "France")); //10 euros
console.log(calculator(5, "Japon")); // 3000 JPY
*/
// frais de livraison V2
function calculatorTwo(poids, destination) {
    if (destination === "France") {
        if (poids <= 1) {
            return "le prix de la livraison est 10 EUR";
        }
        else if (poids > 1 && poids <= 3) {
            return "le prix de la livraison est 20 EUR";
        }
        else if (poids > 3) {
            return "le prix de la livraison est 30 EUR";
        }
    }
    else if (destination === "Canada") {
        if (poids <= 1) {
            return "le prix de la livraison est 15 CAD";
        }
        else if (poids > 1 && poids <= 3) {
            return "le prix de la livraison est 30 CAD";
        }
        else if (poids > 3) {
            return "le prix de la livraison est 45 CAD";
        }
    }
    else if (destination === "Japon") {
        if (poids <= 1) {
            return "le prix de la livraison est 1000 JPY";
        }
        else if (poids > 1 && poids <= 3) {
            return "le prix de la livraison est 2000 JPY";
        }
        else if (poids > 3) {
            return "le prix de la livraison est 3000 JPY";
        }
    }
    else {
        return "Frais de port non définis";
    }
}
console.log(calculatorTwo(1, "France")); //10 euros
console.log(calculatorTwo(5, "Japon")); // 3000 JPY
//todo Ajoute un supplément de 5 EUR, 7.5 CAD, 500 JPY pour les colis dont la somme des dimensions dépasse 150 cm.
// frais de douanes
function douaneTaxes(objectValue, country) {
    if (country === "france") {
        return "pas de frais de douane en France";
    }
    else if (objectValue >= 15 && country === "Canada") {
        var taxeFees = objectValue * 0.15;
        return "les frais de douanes sont de ".concat(taxeFees, " CAD suppl\u00E9mentaires.");
    }
    else if (objectValue >= 5000 && country === "Japon") {
        var taxeFees = objectValue * 0.1;
        return "les frais de douanes sont de ".concat(taxeFees, " JPY suppl\u00E9mentaires.");
    }
    else {
        return "Pays ou valeur non pris en charge pour les frais de douane.";
    }
}
console.log(douaneTaxes(22, "france"));
console.log(douaneTaxes(5600, "Japon"));
// gestion des entrées utilisateur se fera via la ligne de commande
function main() {
    // Demander à l'utilisateur de choisir l'opération
    var readlineSync = require("readline-sync");
    var operation = readlineSync.question("Choisissez une opération (1 pour convertir devises, 2 pour calculer frais de livraison, 3 pour calculer frais de douane): ");
    if (operation === "1") {
        // Opération de conversion de devises
        var montant = parseFloat(readlineSync.question("Entrez le montant à convertir: "));
        var deviseEntree = readlineSync.question("Entrez la devise de départ (EUR, CAD, JPY): ");
        var deviseSortie = readlineSync.question("Entrez la devise de destination (EUR, CAD, JPY): ");
        console.log(deviseEntree, deviseSortie, montant);
        var montantConverti = convert(deviseEntree, deviseSortie, montant);
        console.log("".concat(montantConverti, " ").concat(deviseSortie, "."));
    }
    else if (operation === "2") {
        var poids = parseFloat(readlineSync.question("Entrez le poids du colis en kg: "));
        var destination = readlineSync.question("Entrez le pays de destination (France, Canada, Japon): ");
        var fraisLivraison = calculatorTwo(poids, destination);
        console.log("".concat(fraisLivraison));
    }
    else if (operation === "3") {
        var objectValue = parseFloat(readlineSync.question("Entrez la valeur déclarée du colis: "));
        var country = readlineSync.question("Entrez le pays de destination (France, Canada, Japon): ");
        var fraisDouane = douaneTaxes(objectValue, country);
        console.log("".concat(fraisDouane, " ").concat(country, "."));
    }
    else {
        console.log("Opération non reconnue.");
    }
}
main();
